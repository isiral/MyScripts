import os
import sys
import math
from PlotTemp import PlotNFit

def SortFunc(x):
    return float(x)

Location="TemperatureLogs/"
#ListOfFiles=["Chip1_20.1.LOG","Chip2_20.1.LOG","All_20.1.LOG","Chip1_30.LOG","Chip2_30.LOG","ALL_30.LOG"]

ListOfFiles=os.listdir(Location) 

FinalVal={}
FinalADC={}
FinalErr={}
FinalADCErr={}


BiasRange = [3,4,5,6,7,8]+[14,15]
RegRange =[1,2]

for FName in ListOfFiles:
    if not ".LOG" in FName:
        continue


    File = open(Location+FName)


    TotalReg = [{},{}]
    TotalRegSq = [{},{}]
    TotalADC = [{},{}]
    TotalADCSq = [{},{}]

    Count = [{},{}]
    
    
    

    for Chan in range(0,2):
        for reg in RegRange:
            TotalReg[Chan][reg]=0.
            TotalRegSq[Chan][reg]=0.
            Count[Chan][reg]=0
            TotalADC[Chan][reg]=0.
            TotalADCSq[Chan][reg]=0.


            
        for reg in BiasRange:
            TotalReg[Chan][reg]=[0.,0.]
            TotalRegSq[Chan][reg]=[0.,0.]
            Count[Chan][reg]=[0,0]
            TotalADC[Chan][reg]=[0.,0.]
            TotalADCSq[Chan][reg]=[0.,0.]

            
    #print (Location+FName)


    Chan=0
    for line in File:

        if "65535" in line:
            continue
        
        if "Measuring for FE:" in line:
            Chan = int(line.split("FE:")[1])
        if Chan==2:
            Chan=1


        for reg in RegRange + BiasRange:
            if ("MUX_V: "+str(reg))+" " in line: 
                #print line
                if "Value" in line:
                    Val = float(line.split()[6])
                    TotalReg[Chan][reg]+=Val
                    TotalRegSq[Chan][reg]+=Val*Val
                    Count[Chan][reg]+=1

                    ADC = float(line.split()[4])
                    TotalADC[Chan][reg]+=ADC
                    TotalADCSq[Chan][reg]+=ADC*ADC
                    

                    #print(Val) 
                
                elif "Bias" in line:
                    Val = [float(line.split()[7]),float(line.split()[13])]
                    ADC = [float(line.split()[5]),float(line.split()[11])]
                    #print Val,reg
                    for ind in range(0,2):
                        TotalReg[Chan][reg][ind]+=Val[ind]
                        TotalRegSq[Chan][reg][ind]+=Val[ind]*Val[ind]
                        Count[Chan][reg][ind]+=1
                        TotalADC[Chan][reg][ind]+=ADC[ind]
                        TotalADCSq[Chan][reg][ind]+=ADC[ind]*ADC[ind]
                    
    # print (FName)
    tFName = FName.replace(".LOG","").split("_")
    TMP = tFName[1]
    TYP = tFName[0].lower()

    SaveLoc=0
    if TYP in "chip1":
        SaveLoc=0
    elif TYP in "chip2":
        SaveLoc=1
    elif TYP in "all":
        SaveLoc=2
        
    # print (TYP)
    if not TMP in FinalVal.keys():
        FinalVal[TMP]={}
        FinalADC[TMP]={}
        FinalErr[TMP]={}
        FinalADCErr[TMP]={}

    for Chan in range(0,2):
        # print (Chan)
        for reg in BiasRange:
            if not reg in FinalVal[TMP].keys():
                FinalVal[TMP][reg]=[0,0,0,0]
                FinalErr[TMP][reg]=[0,0,0,0]
                FinalADC[TMP][reg]=[0,0,0,0]
                FinalADCErr[TMP][reg]=[0,0,0,0]

            if Count[Chan][reg][0]>0:
                Values = (TotalReg[Chan][reg][1]/Count[Chan][reg][1]-TotalReg[Chan][reg][0]/Count[Chan][reg][0])   #max(TotalReg[reg][0].iteritems(),key=lambda x: x[1])[0], 
                Err1 =  math.fabs((TotalReg[Chan][reg][0]/Count[Chan][reg][0])*(TotalReg[Chan][reg][0]/Count[Chan][reg][0]) - (TotalRegSq[Chan][reg][0]/Count[Chan][reg][0]))
                Err2 =  math.fabs((TotalReg[Chan][reg][1]/Count[Chan][reg][1])*(TotalReg[Chan][reg][1]/Count[Chan][reg][1]) - (TotalRegSq[Chan][reg][1]/Count[Chan][reg][1]))
                Err = math.sqrt(Err1+Err2)


                ADC = (TotalADC[Chan][reg][1]/Count[Chan][reg][1]-TotalADC[Chan][reg][0]/Count[Chan][reg][0])   #max(TotalADC[reg][0].iteritems(),key=lambda x: x[1])[0], 
                ADCErr1 =  math.fabs((TotalADC[Chan][reg][0]/Count[Chan][reg][0])*(TotalADC[Chan][reg][0]/Count[Chan][reg][0]) - (TotalADCSq[Chan][reg][0]/Count[Chan][reg][0]))
                ADCErr2 =  math.fabs((TotalADC[Chan][reg][1]/Count[Chan][reg][1])*(TotalADC[Chan][reg][1]/Count[Chan][reg][1]) - (TotalADCSq[Chan][reg][1]/Count[Chan][reg][1]))
                ADCErr = math.sqrt(Err1+Err2)

                tmpChan=0
                if TYP in "all":
                    tmpChan=Chan

                FinalVal[TMP][reg][SaveLoc+tmpChan]=Values
                FinalErr[TMP][reg][SaveLoc+tmpChan]=Err
                FinalADC[TMP][reg][SaveLoc+tmpChan]=ADC
                FinalADCErr[TMP][reg][SaveLoc+tmpChan]=ADCErr

    for Chan in range(0,2):
        for reg in RegRange:
            if not reg in FinalVal[TMP].keys():
                FinalVal[TMP][reg]=[0,0,0,0]
                FinalErr[TMP][reg]=[0,0,0,0]
                FinalADC[TMP][reg]=[0,0,0,0]
                FinalADCErr[TMP][reg]=[0,0,0,0]

            if Count[Chan][reg]>0:
                Values = TotalReg[Chan][reg]/Count[Chan][reg]
                Err = math.sqrt(math.fabs( Values*Values - TotalRegSq[Chan][reg]/Count[Chan][reg]))
                ADC = TotalADC[Chan][reg]/Count[Chan][reg]
                ADCErr = math.sqrt(math.fabs( ADC*ADC - TotalADCSq[Chan][reg]/Count[Chan][reg]))
                
                
                tmpChan=0
                if TYP in "all":
                    tmpChan=Chan
                FinalVal[TMP][reg][SaveLoc+tmpChan]=Values
                FinalErr[TMP][reg][SaveLoc+tmpChan]=Err
                FinalADC[TMP][reg][SaveLoc+tmpChan]=ADC
                FinalADCErr[TMP][reg][SaveLoc+tmpChan]=ADCErr
                
           





# for reg in BiasRange:
#     print()
#     print( "Register:",reg)

#     for TMP in sorted(FinalVal.keys()):
#         print (TMP, FinalVal[TMP][reg][0],FinalErr[TMP][reg][0],FinalVal[TMP][reg][1],FinalErr[TMP][reg][1],FinalVal[TMP][reg][2],FinalErr[TMP][reg][2],FinalVal[TMP][reg][3],FinalErr[TMP][reg][3])
        







FitVal={}
for reg in BiasRange + RegRange:

    DataSetChan1 = {}
    DataSetChan2 = {}
    DataSetAChan1 = {}
    DataSetAChan2 = {}

    DataSetADCChan1 = {}
    DataSetADCChan2 = {}
    DataSetADCAChan1 = {}
    DataSetADCAChan2 = {}

    Scale=1
    Fit=True
    if reg ==1:
        Scale=1.05
        Fit=False
    if reg ==2:
        Scale=1.005
        Fit=False
        
    for TMP in sorted(FinalVal.keys()):
        DataSetChan1[TMP]=[FinalVal[TMP][reg][0],FinalErr[TMP][reg][0]]
        DataSetChan2[TMP]=[FinalVal[TMP][reg][1],FinalErr[TMP][reg][1]]
        DataSetAChan1[TMP]=[FinalVal[TMP][reg][2],FinalErr[TMP][reg][2]]
        DataSetAChan2[TMP]=[FinalVal[TMP][reg][3],FinalErr[TMP][reg][3]]
        DataSetADCChan1[TMP]=[FinalADC[TMP][reg][0],FinalADCErr[TMP][reg][0]]
        DataSetADCChan2[TMP]=[FinalADC[TMP][reg][1],FinalADCErr[TMP][reg][1]]
        DataSetADCAChan1[TMP]=[FinalADC[TMP][reg][2],FinalADCErr[TMP][reg][2]]
        DataSetADCAChan2[TMP]=[FinalADC[TMP][reg][3],FinalADCErr[TMP][reg][3]]

        
        

    PlotNFit([DataSetChan1], str(reg)+" Chan1",["Chip1"],Scale,Fit)
    PlotNFit([DataSetChan2], str(reg)+" Chan2",["Chip2"],Scale,Fit)

    PlotNFit([DataSetAChan1], str(reg)+" AChan1",["Chip1 - Meas 2"],Scale,Fit)
    PlotNFit([DataSetAChan2], str(reg)+" AChan2",["Chip2 - Meas 2"],Scale,Fit)


    FitVal[reg] = PlotNFit([DataSetChan1,DataSetChan2], str(reg)+" Chan1 Chan2",["Chip 1","Chip 2"],Scale,Fit)
    
    PlotNFit([DataSetChan1,DataSetAChan1], str(reg)+" Chan1 AChan1",["Meas 1","Meas 2"],Scale,Fit)
    PlotNFit([DataSetChan2,DataSetAChan2], str(reg)+" Chan2 AChan2",["Meas 1","Meas 2"],Scale,Fit)

    PlotNFit([DataSetChan1,DataSetChan2,DataSetAChan1,DataSetAChan2], str(reg)+" AllPlots",["Chip 1 - Meas 1"," Chip 2 - Meas 1","Chip 1 - Meas 2"," Chip 2 - Meas 2"],Scale,Fit)
    PlotNFit([DataSetADCChan1,DataSetADCChan2,DataSetADCAChan1,DataSetADCAChan2], str(reg)+" AllPlots_ADC",["Chip 1 - Meas 1"," Chip 2 - Meas 1","Chip 1 - Meas 2"," Chip 2 - Meas 2"],Scale,Fit)

print
print("Register","Chip","p0","Err0","p1","Err1")
for reg in BiasRange+ RegRange:
    if len(FitVal[reg])>0:
        for chip in range(0,2):
            print (reg,chip,FitVal[reg][chip][0],FitVal[reg][chip][1],FitVal[reg][chip][2],FitVal[reg][chip][3])
        print()


print
print ("Switched ADC -> Temperature fit values")
print("Register","Chip","p0","p1")
for chip in range(0,2):
    print ("Chip:",chip)
    for reg in BiasRange:
        if len(FitVal[reg])>0:
            p0 = -1 * FitVal[reg][chip][0]/FitVal[reg][chip][2]
            p1 = 1/FitVal[reg][chip][2]

            
            Name={ 3:"\"TempSen0Par\"",4:"\"RadSen0Par\"",
                   5:"\"TempSen1Par\"",6:"\"RadSen1Par\"",
                   7:"\"TempSen2Par\"",8:"\"RadSen2Par\"",
                   15:"\"TempSen3Par\"",14:"\"RadSen3Par\""}
                   
      
            print (Name[reg]+":[",p0,",",p1,"],")
        


print("Average Temperature Noise")
for reg in BiasRange+ RegRange:
    if len(FitVal[reg])>0:
        for chip in range(0,2):
            p1 = 1/FitVal[reg][chip][2]
            Noise=[]
            for Temp in sorted(FinalErr.keys(), key=SortFunc):
                Noise.append(p1*FinalErr[Temp][reg][chip])
            print (reg,chip,"Max Noise: ",max(Noise))
        print()
