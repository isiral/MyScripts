import ROOT

Range = [13, -32.5,32.5 ]


def PlotNFit( DataSetList, RegName="",HistName=[], Scale=1, DoFit=True):


    ROOT.gROOT.SetBatch(True)
    ROOT.gStyle.SetOptStat(0)

    Canvas = ROOT.TCanvas()
    legend = ROOT.TLegend(0.11,0.70,0.6,0.89)
    
    ColorInd=-1;
    Colors=[ROOT.kRed,ROOT.kBlack,ROOT.kGreen,ROOT.kBlue]

    Histograms = []
    FitValues = []
    for DataSet in DataSetList:
        ColorInd+=1
        Histograms.append(ROOT.TH1F("Register"+str(ColorInd)+RegName.replace(" ","") , "Register"+str(ColorInd)+RegName, Range[0],Range[1],Range[2]))
        StepSize = (Range[2]-Range[1])/Range[0]



        for Temp in DataSet.keys():
            Bin = int((float(Temp)-Range[1])/StepSize) +1
            Histograms[-1].SetBinContent( Bin  , DataSet[Temp][0])
            Histograms[-1].SetBinError( Bin , DataSet[Temp][1])


        Histograms[-1].SetLineColor(Colors[ColorInd])
        Histograms[-1].SetMarkerColor(Colors[ColorInd])

        
            
        SAME=""
        if not ColorInd==0:
            SAME=" SAME"


        Histograms[-1].SetTitle("")    
        Histograms[-1].GetXaxis().SetTitle("Temperature (C)")
        Histograms[-1].GetYaxis().SetTitle("ADC Voltage (V)")
        Histograms[-1].SetMaximum(Histograms[-1].GetMaximum() * Scale)
        Histograms[-1].Draw(SAME)

        if DoFit:
            Histograms[-1].Fit("pol1","0")
            Fit = Histograms[-1].GetFunction("pol1")
            Fit.SetLineColor(Colors[ColorInd])
            Fit.Draw("SAME")


            
        if len(HistName)>ColorInd:
            if DoFit:
                legend.AddEntry(Histograms[-1],HistName[ColorInd]+" p0: "+"{:.2E}".format(Fit.GetParameter(0))+" p1: "+"{:.2E}".format(Fit.GetParameter(1)),"l")
                FitValues.append([Fit.GetParameter(0),Fit.GetParError(0),Fit.GetParameter(1),Fit.GetParError(1)])
            else:
                legend.AddEntry(Histograms[-1],HistName[ColorInd],"l")
                
        

    legend.SetBorderSize(0)
    legend.Draw()
    Canvas.SaveAs("Register"+RegName.replace(" ","")+".png")
        
    return FitValues
