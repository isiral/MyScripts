import math
import ROOT

def ReadDat( DatFile):
    DataMap = [ [],[] ]
    Ranges = [0.0,0.0,0.0,0.0,0.0,0.0]
    Transpose = [ [], [] ]
    for ind in range(0,2):

        File = DatFile[ind]

        LineCount=0
        for line in File:
            LineCount+=1

            if LineCount<9:

                if LineCount==6:
                    Ranges[0] = int(line.split()[0])
                    Ranges[1] = float(line.split()[1])
                    Ranges[2] = float(line.split()[2])
                elif LineCount==7:
                    Ranges[3] = int(line.split()[0])
                    Ranges[4] = float(line.split()[1])
                    Ranges[5] = float(line.split()[2])

                continue

            DataMap[ind].append(line.split())

        YAxis = len(DataMap[ind])
        XAxis = len(DataMap[ind][0])

        Transpose[ind] = [[DataMap[ind][j][i] for j in range(len(DataMap[ind]))] for i in range(len(DataMap[0][ind]))]
    return Transpose,Ranges

    #return DataMap, Ranges 


#Read the Histograms in the the PixelMap 3d Matrix
ThresholdFile = [open("001137_diff_thresholdscan/JohnDoe_0_ThresholdMap-0.dat") ,open("001138_diff_smartscan/JohnDoe_0_ThresholdMap-0.dat")]
ThresholdMap,ThresholdRange  = ReadDat(ThresholdFile)
ThresholdName = "Threshold"

Divide=True
Chi2Norm = 1

DataFile = [open("001137_diff_thresholdscan/JohnDoe_0_NoiseMap-0.dat") ,open("001138_diff_smartscan/JohnDoe_0_NoiseMap-0.dat")]

PixelMap,PixelRange  = ReadDat(DataFile)
#PixelMap,PixelRange = ThresholdMap,ThresholdRange


PixelName = "NoiseMap"




#print (PixelRange)

#RangeX = [ 80, 00, 4000]
RangeX = [ 100, 00, 200]

#RangeY = [ 100, 0, 100]
RangeY = [ 300, 0, 30]


OutputFile = open("OutputFile.log","w+")

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)



## Calculate the differences

DiffMap = [ [0.]*len(PixelMap[0][0]) ] *len(PixelMap[0])
rFile = ROOT.TFile("Output.root","recreate")
hDiff = ROOT.TH2F("Differences","Differences",PixelRange[0],PixelRange[1],PixelRange[2],PixelRange[3],PixelRange[4],PixelRange[5])
hCorrelation = ROOT.TH2F("Correlation","Correlation",RangeX[0],RangeX[1],RangeX[2],RangeX[0],RangeX[1],RangeX[2])

hCor1 = ROOT.TH1F("CorrelationsSTD","CorrelationsSTD",RangeX[0],RangeX[1],RangeX[2])
hCor2 = ROOT.TH1F("CorrelationsSMRT","CorrelationsSMRT",RangeX[0],RangeX[1],RangeX[2])
h2dCor1 = ROOT.TH2F("Correlations2DSTD","Correlations2DSTD",RangeX[0],RangeX[1],RangeX[2],RangeY[0],RangeY[1],RangeY[2])
h2dCor2 = ROOT.TH2F("Correlations2DSMRT","Correlations2DSMRT",RangeX[0],RangeX[1],RangeX[2],RangeY[0],RangeY[1],RangeY[2])
#HistoCount = ROOT.TH1F("Count","Count",RangeX[0],0,400)


for Xind in range(0,len(PixelMap[0])):
    for Yind in range(0,len(PixelMap[0][0])):

        if ( float(PixelMap[0][Xind][Yind])>0):
            DiffMap[Xind][Yind] = math.fabs(float(PixelMap[1][Xind][Yind])/Chi2Norm -float(PixelMap[0][Xind][Yind]))
            if (Divide):
                DiffMap[Xind][Yind] = DiffMap[Xind][Yind]/float(PixelMap[0][Xind][Yind])*100

        elif (float(PixelMap[1][Xind][Yind])>0):
           DiffMap[Xind][Yind] = math.fabs(float(PixelMap[1][Xind][Yind])/Chi2Norm -float(PixelMap[0][Xind][Yind]))
           if (Divide):
                DiffMap[Xind][Yind] = DiffMap[Xind][Yind]/float(PixelMap[1][Xind][Yind])*100
        else:
            DiffMap[Xind][Yind]=0



        hCorrelation.Fill( float(PixelMap[0][Xind][Yind]), float(PixelMap[1][Xind][Yind]) )


        print ("Xind:",Xind,"Yind:",Yind,"Value:",float(PixelMap[0][Xind][Yind]),float(PixelMap[1][Xind][Yind]), float(DiffMap[Xind][Yind]))
        

        if (float(DiffMap[Xind][Yind])==100):
            DiffMap[Xind][Yind]=0.0001

        if (float(DiffMap[Xind][Yind])>100):
            DiffMap[Xind][Yind]=0.0001

            
        hDiff.SetBinContent(Xind+1,Yind+1,float(DiffMap[Xind][Yind]))
        hCor1.Fill(float(ThresholdMap[0][Xind][Yind]),math.fabs(DiffMap[Xind][Yind]))
        hCor2.Fill(float(ThresholdMap[1][Xind][Yind]),math.fabs(DiffMap[Xind][Yind]))

        h2dCor1.Fill(float(ThresholdMap[0][Xind][Yind]),math.fabs(DiffMap[Xind][Yind]),1)
        h2dCor2.Fill(float(ThresholdMap[1][Xind][Yind]),math.fabs(DiffMap[Xind][Yind]),1)

        #


       

                  
    print(" ".join(str(x) for x in DiffMap[Xind]),file=OutputFile)

Canvas = ROOT.TCanvas()
hDiff.GetXaxis().SetTitle("Column")
hDiff.GetYaxis().SetTitle("Row")
#hDiff.GetZaxis().SetRange(0,30)
hDiff.Draw("COLZ")
Canvas.SaveAs("OutputPlot.png")


hCorrelation.Draw("COLZ")
hCorrelation.GetXaxis().SetTitle(PixelName+" Original Scan")
hCorrelation.GetYaxis().SetTitle(PixelName+" Smart Scan")
Canvas.SaveAs("OutputCor.png")

hCor1.GetXaxis().SetTitle(ThresholdName+" Original Scan")
hCor1.GetYaxis().SetTitle(PixelName)
hCor1.Draw()
Canvas.SaveAs("OutputhSTD.png")

hCor2.GetXaxis().SetTitle(ThresholdName+" Smart Scan")
hCor2.GetYaxis().SetTitle(PixelName)
hCor2.Draw()
Canvas.SaveAs("OutputhSMRT.png")


h2dCor1.GetXaxis().SetTitle(ThresholdName+" Original Scan")
h2dCor1.GetYaxis().SetTitle(PixelName)
h2dCor1.Draw("COLZ")
Canvas.SaveAs("Output2dhSTD.png")

h2dCor2.GetXaxis().SetTitle(ThresholdName+" Smart Scan")
h2dCor2.GetYaxis().SetTitle(PixelName)
h2dCor2.Draw("COLZ")
Canvas.SaveAs("Output2dhSMRT.png")


rFile.Close()
    
        
